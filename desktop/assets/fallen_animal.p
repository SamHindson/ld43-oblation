Untitled
- Delay -
active: false
- Duration - 
lowMin: 6000.0
lowMax: 6000.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 250.0
highMax: 250.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.061643835
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 3000.0
highMax: 6000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.3529412
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.42465752
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8039216
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.020547945
timeline2: 0.8356164
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 10.0
highMin: 30.0
highMax: 300.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.05479452
- Angle - 
active: true
lowMin: 90.0
lowMax: 90.0
highMin: 0.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.999
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 0.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.8235294
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.24313726
colors1: 0.24313726
colors2: 0.24313726
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9649123
scaling2: 0.15789473
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.09589041
timeline2: 0.3219178
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/D:/Documents/Downloads/m2pac/doot.png


Untitled
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 5
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.02739726
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 6000.0
highMax: 6000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 100.0
highMin: 80.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 720.0
highMax: 720.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1849315
timeline2: 1.0
- Wind - 
active: false
- Gravity - 
active: true
lowMin: -3000.0
lowMax: -3000.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.32941177
colors1: 0.30980393
colors2: 0.30588236
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/D:/Documents/Downloads/m2pac/doot.png

