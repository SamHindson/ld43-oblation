package co.sam.sacrifices.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import co.sam.sacrifices.LD43Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = (int) (config.width * 0.75f);
		config.resizable = false;
		config.title = "O B L A T I O N";
		new LwjglApplication(new LD43Game(), config);
	}
}