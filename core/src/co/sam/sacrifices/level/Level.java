package co.sam.sacrifices.level;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import co.sam.sacrifices.entities.Altar;
import co.sam.sacrifices.entities.Animal;
import co.sam.sacrifices.entities.DemonBlock;
import co.sam.sacrifices.entities.Demonic;
import co.sam.sacrifices.entities.Duck;
import co.sam.sacrifices.entities.Endgame;
import co.sam.sacrifices.entities.Entity;
import co.sam.sacrifices.entities.Goat;
import co.sam.sacrifices.entities.MrBill;
import co.sam.sacrifices.entities.RockPile;
import co.sam.sacrifices.entities.Sign;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public class Level {

	public static final int BLOCK_WIDTH = 32;

	static final int GROUND = 0xff;
	static final int AIR = 0xffffffff;
	static final int LEFT_INCLINE = 0xccccc0ff;
	static final int RIGHT_INCLINE = 0xccccc1ff;
	static final int ALTAR = 0x66;
	static final int ROCKS = 0x77;
	static final int DEMONBLOCK = 0x55;

	static final int DUCK = 0xff0000ff;
	static final int GOAT = 0xff0088ff;
	static final int YOU = 0xab0c00ff;

	static final int SPAWN = 0x0000ffff;

	static final int PEEKABOO = 0x00ff00ff;
	static final int SIGN = 0x69;

	static final int ENDGAME = 0x123;

	static final int CHECKPOINT = 0x330099ff;

	public static final float GRAVITY = 300;

	int[] groundlevel;
	int w, h;
	float playerSpawnX, playerSpawnY;

	boolean removedSomething = false;

	ArrayList<Entity> entities;
	ArrayList<Integer> levelJumps;

	PlayScreen host;

	private Texture undergroundBlock;

	float peekabooX = -Integer.MAX_VALUE / 2, peekabooY;
	boolean peeked, peeking;
	float peekProgress;
	Sprite peekSprite;
	Sound scaryBoiSound;

	ParticleEffectPool oofClouds;
	ParticleEffectPool creationClouds;
	Array<PooledEffect> effects = new Array();

	Array<Vector2> checkpoints = new Array<>();
	
	Pixmap schema;

	public Level(PlayScreen host, Player player) {
		this.host = host;

		entities = new ArrayList<Entity>();

		// Load schematics from resources
		schema = new Pixmap(Gdx.files.internal("level.png"));

		w = schema.getWidth();
		h = schema.getHeight();

		groundlevel = new int[schema.getWidth()];

		for (int x = 0; x < w; x++) {
			for (int yy = 0; yy < h; yy++) {
				int p = schema.getPixel(x, yy);
				int y = h - yy - 1;

				// Find out where the ground is
				if (p == GROUND)
					groundlevel[x] = y;

				// Create entities
				else if (p == DUCK) {
					entities.add(new Duck(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH));
				} else if (p == GOAT) {
					entities.add(new Goat(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH));
				} else if (((p & 0xff000000) >> 24) == ALTAR) {
					int altarID = (p & 0x000fff00) >> 8;
					int needs = (p & 0x00f00000) >> 20;
					entities.add(0, new Altar(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH, altarID, needs));
				} else if (((p & 0xff000000) >> 24) == ROCKS) {
					int rockID = (p & 0x000fff00) >> 8;
					entities.add(0, new RockPile(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH, rockID));
				} else if (p == SPAWN) {
					playerSpawnX = x * BLOCK_WIDTH;
					playerSpawnY = y * BLOCK_WIDTH;
					checkpoints.add(new Vector2(x * BLOCK_WIDTH, y * BLOCK_WIDTH));
				} else if (p == PEEKABOO) {
					peekabooX = x * BLOCK_WIDTH;
					peekabooY = y * BLOCK_WIDTH;
				} else if (((p & 0xff000000) >> 24) == SIGN) {
					int signID = ((p & 0x00ffff00) >> 8);
					entities.add(new Sign(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH, signID));
				} else if (((p & 0xff000000) >> 24) == DEMONBLOCK) {
					int rockID = (p & 0x000fff00) >> 8;
					entities.add(0, new DemonBlock(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH, rockID));
				} else if (p == YOU) {
					entities.add(new MrBill(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH));
				} else if (((p & 0xfff00000) >> 20) == ENDGAME) {
					int rockID = (p & 0x000fff00) >> 8;
					entities.add(new Endgame(this, host, x * BLOCK_WIDTH, y * BLOCK_WIDTH, rockID));
				} else if (p == CHECKPOINT) {
					checkpoints.add(new Vector2(x * BLOCK_WIDTH, y * BLOCK_WIDTH));
				}
			}
		}

		// TODO link rocks and altars
		for (Entity e : entities) {
			if (e instanceof Altar) {
				boolean pileFound = false;
				for (Entity f : entities) {
					if (f instanceof Demonic) {
						if (((Demonic) f).getID() == ((Altar) e).getID()) {
							((Altar) e).setLinkedEntity(((Demonic) f));
							pileFound = true;
							break;
						}
					}
					if (pileFound)
						break;
				}
			}
		}

		// Set the player down
		player.setPosition(playerSpawnX, playerSpawnY);

		Color underground = new Color(0x2b2b2bff);
		Pixmap p3 = new Pixmap(1, 1, Format.RGBA8888);
		p3.setColor(underground);
		p3.drawPixel(0, 0);
		undergroundBlock = new Texture(p3);

		calculateLevelJumps();

		// oof

		ParticleEffect oofCloud = new ParticleEffect();
		oofCloud.load(Gdx.files.internal("fallen_animal.p"), host.getAtlas());

		oofClouds = new ParticleEffectPool(oofCloud, 1, 2);

		ParticleEffect creationCloud = new ParticleEffect();
		creationCloud.load(Gdx.files.internal("block_create.p"), host.getAtlas());

		creationClouds = new ParticleEffectPool(creationCloud, 1, 2);

		peekSprite = host.getSprite("scaryboi");
		peekSprite.setScale(4);

		// Scerry sound
		scaryBoiSound = Gdx.audio.newSound(Gdx.files.internal("scaryboi2.ogg"));
	}

	public void resetToCheckpoint(Player player) {
		Array<Vector2> pointsBehind = new Array<>();
		Vector2 chosen = null;
		float minDist = Integer.MAX_VALUE;
		for(Vector2 v : checkpoints) {
			if(v.x < player.getX()) {
				if(player.getX() - v.x < minDist) {
					chosen = v;
					minDist = player.getX() - v.x;
				}
			}
		}
		if(chosen != null) {
			for(Entity e : entities) {
				e.reset();
			}
			player.setPosition(chosen.x, chosen.y);
			player.clearFollowers();
			
			//	Do heightmap stuff
			for (int x = 0; x < w; x++) {
				for (int yy = 0; yy < h; yy++) {
					int p = schema.getPixel(x, yy);
					int y = h - yy - 1;

					// Find out where the ground is
					if (p == GROUND)
						groundlevel[x] = y;
				}
			}
		}
	}

	public float getPlayerSpawnX() {
		return playerSpawnX;
	}

	public float getPlayerSpawnY() {
		return playerSpawnY;
	}

	void calculateLevelJumps() {
		levelJumps = new ArrayList<>();
		levelJumps.add(0);
		int b = groundlevel[0];
		for (int i = 1; i < groundlevel.length; i++) {
			if (groundlevel[i] != groundlevel[i - 1]) {
				levelJumps.add(i);
			}
		}
		levelJumps.add(w);
	}

	public void update(float dt, OrthographicCamera camera) {
		// ?
		for (Entity e : entities) {
			Vector3 screenCoords = camera.project(new Vector3(e.getX(), e.getY(), 0));

			if (!(screenCoords.x < -BLOCK_WIDTH || screenCoords.x > Gdx.graphics.getWidth())) {
				e.update(dt, this);
			}

			if (removedSomething) {
				removedSomething = false;
				break;
			}
		}

		if (peeking) {
			peekProgress += dt;
			if (peekProgress >= 1) {
				peeking = false;
				peeked = true;
			}
		}
	}

	public void makeOofCloud(float x, float y) {
		PooledEffect effect = oofClouds.obtain();
		effect.setPosition(x, y);
		effect.start();
		effects.add(effect);
	}

	public void makeCreationCloud(float x, float y) {
		PooledEffect effect = creationClouds.obtain();
		effect.setPosition(x, y);
		effect.start();
		effects.add(effect);
	}

	int getGrassVar(int x) {
		return (x * 17 + 35) % 3 + 1;
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {
		// Draw background
		if (peeking) {
			float yeh = 0;
			if (peekProgress < 0.15f)
				yeh = peekProgress * 26;
			else if (peekProgress > 0.75f)
				yeh = 4 + (peekProgress - 0.75f) * -26;
			else
				yeh = 4;

			peekSprite.setPosition(peekabooX + 7 * BLOCK_WIDTH, peekabooY + peekSprite.getHeight() * (yeh - 3));
			peekSprite.draw(batch);
		}

		// Draw floor
		for (int x = 0; x < w; x++) {
			Vector3 screenCoords = camera.project(new Vector3(x * BLOCK_WIDTH, groundlevel[x] * BLOCK_WIDTH, 0));

			if (!(screenCoords.x < -BLOCK_WIDTH * 2 || screenCoords.x > Gdx.graphics.getWidth())) {
				int var = getGrassVar(x);
				batch.draw(host.getTextureRegion("ground" + var), x * BLOCK_WIDTH, groundlevel[x] * BLOCK_WIDTH);
			}
		}

		// Draw the v o i d
		for (int i = 0; i < levelJumps.size() - 1; i++) {
			int hhh = Gdx.graphics.getWidth();
			int yyy = groundlevel[levelJumps.get(i)] * BLOCK_WIDTH;
			batch.draw(undergroundBlock, levelJumps.get(i) * BLOCK_WIDTH, yyy - hhh,
					(levelJumps.get(i + 1) - levelJumps.get(i)) * BLOCK_WIDTH, hhh);
		}

		// Draw Entities
		for (Entity e : entities) {
			Vector3 screenCoords = camera.project(new Vector3(e.getX(), e.getY(), 0));

			if ((screenCoords.x > -e.getW() * 2) && (screenCoords.x < Gdx.graphics.getWidth())) {
				e.draw(batch);
			}
		}

		// Draw effetcs
		for (int i = effects.size - 1; i >= 0; i--) {
			PooledEffect effect = effects.get(i);
			effect.draw(batch, Gdx.graphics.getDeltaTime());
			if (effect.isComplete()) {
				effect.free();
				effects.removeIndex(i);
			}
		}
	}

	public float groundHeightAt(float x) {
		int bx = (int) (x / 32.f);
		try {
			return groundlevel[bx];
		} catch (ArrayIndexOutOfBoundsException e) {
			return 0;
		}
	}

	public void debugRender(ShapeRenderer debugRenderer) {
		// Draw block center?
		for (int x = 0; x < w; x++) {
			if (MathUtils.randomBoolean(0.3f))
				debugRenderer.rect(x * BLOCK_WIDTH, groundlevel[x] * BLOCK_WIDTH, BLOCK_WIDTH * 0.1f,
						BLOCK_WIDTH * 0.1f);
		}

		for (Entity e : entities) {
			e.debugRender(debugRenderer);
		}
	}

	public float getPeekabooX() {
		return peekabooX;
	}

	public float getPeekabooY() {
		return peekabooY;
	}

	public boolean[] canTraverse(float x, float y, float w) {
		boolean l = true;
		boolean r = true;
		/*if (groundHeightAt(x + BLOCK_WIDTH) > groundHeightAt(x) && y <= groundHeightAt(x + BLOCK_WIDTH) * BLOCK_WIDTH) {
			r = false;
		}
		if (groundHeightAt(x - 1) > groundHeightAt(x) && y <= groundHeightAt(x - BLOCK_WIDTH) * BLOCK_WIDTH) {
			l = false;
		}*/
		int px = (int)(x/BLOCK_WIDTH);
		int py = (int)((y+4)/BLOCK_WIDTH) - 1;
		
		/*if (groundHeightAt(x + BLOCK_WIDTH) > groundHeightAt(x) && py <= groundHeightAt(x + BLOCK_WIDTH)) {
			r = false;
		}
		if (groundHeightAt(x - 1) > groundHeightAt(x) && y <= groundHeightAt(x - BLOCK_WIDTH) * BLOCK_WIDTH) {
			l = false;
		}*/
		
		if(py < groundHeightAt(x + w + 5))
			r = false;
		if(py < groundHeightAt(x - 5)) {
			l = false;
		}
		
		if (x < 0)
			l = false;
		if (x + w > this.w * BLOCK_WIDTH)
			r = false;

		// Check for rocks
		for (Entity entity : entities) {
			if (entity instanceof RockPile) {
				if (Math.abs(x - entity.getX()) < 10 && !((RockPile) entity).isLifted()) {
					r = false;
				}
			}
		}

		return new boolean[] { l, r };
	}

	public void peek() {
		if (!peeked) {
			scaryBoiSound.play(0.2f);
			peeking = true;
			peeked = true;
			peekProgress = 0;
		}
	}

	public boolean canFall(float x, float y, float dx, float dy, float w2, float h2) {
		if (dy > 0) {
			return true;
		}
		return y > (groundHeightAt(x + w2/4) + 1) * BLOCK_WIDTH && y > (groundHeightAt(x + w2*0.75f) + 1) * BLOCK_WIDTH;
	}

	public ArrayList<Entity> getEntities() {
		return entities;
	}

	public void killAnimal(Animal animal) {
		for (Entity e : entities) {
			if (e.equals(animal)) {
				host.addInMemoryOf(animal.getPetName() + animal.getTypeString());
				makeOofCloud(animal.getX(), animal.getY());
				removedSomething = true;
				break;
			}
		}
	}

	public void addFloorBlock(float x, float y) {
		int bx = (int) (x / 32.f);
		int by = (int) (y / 32.f);
		groundlevel[bx] = by;
		calculateLevelJumps();
	}

	public void destroyEntities() {
		for (Entity e : entities) {
			e.destroy();
		}
		entities.clear();
		removedSomething = true;
	}

	public float getWidth() {
		return w * BLOCK_WIDTH;
	}

	public float getHeight() {
		return h * BLOCK_WIDTH;
	}
}
