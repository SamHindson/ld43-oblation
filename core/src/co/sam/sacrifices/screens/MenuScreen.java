package co.sam.sacrifices.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Align;

import co.sam.sacrifices.LD43Game;

public class MenuScreen extends ScreenAdapter {

	BitmapFont font;
	SpriteBatch batch;

	String text = "a game by Sam Hindson\ncreated for Ludum Dare 43: Sacrifices Must Be Made\n\n[A] Left\n[D] Right\n[SPACE] Jump\n[J] Use\n[K] Cancel\n[R] Reset\n\nPress J";

	Music music;

	Texture logo;

	LD43Game game;

	public MenuScreen(LD43Game game) {
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("homocide.fnt"));
		logo = new Texture(Gdx.files.internal("logo.png"));
		this.game = game;
		music = Gdx.audio.newMusic(Gdx.files.internal("endgame_song.ogg"));
		music.setVolume(0.3f);
		music.setLooping(true);
		try {
			music.play();
		} catch (Exception e) {
			System.err.println("Music's screwign around again.");
		}
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		LD43Game.a = MathUtils.random(0.05f);
		batch.begin();
		batch.draw(logo, Gdx.graphics.getWidth() / 2 - logo.getWidth() / 2,
				Gdx.graphics.getHeight() - 35 - logo.getHeight());
		font.draw(batch, text, Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() * 0.7f,
				Gdx.graphics.getWidth() / 2, Align.center, false);
		batch.end();

		if (Gdx.input.isKeyJustPressed(Keys.J)) {
			music.stop();
			game.openPlayScreen();
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		music.dispose();
		batch.dispose();
		logo.dispose();
	}
}
