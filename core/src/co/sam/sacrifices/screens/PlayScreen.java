package co.sam.sacrifices.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import co.sam.sacrifices.LD43Game;
import co.sam.sacrifices.entities.Altar;
import co.sam.sacrifices.entities.Animal;
import co.sam.sacrifices.entities.CameraTarget;
import co.sam.sacrifices.entities.Entity;
import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.HUD;
import co.sam.sacrifices.player.Player;

public class PlayScreen extends ScreenAdapter {

	public static final int ICONSIZE = 40;

	Level level;
	TextureAtlas atlas;
	SpriteBatch worldBatch, hudBatch, demonBatch;
	ShapeRenderer debugRenderer;

	OrthographicCamera camera;

	Player player;
	HUD hud;

	String notificationText = "";
	float notificationTime;

	BitmapFont standardFont, cursedFont;
	GlyphLayout cursedGlyph;
	Texture dimmingBlock, lightenBlock;
	NinePatch sacrificeBackground;
	NinePatch outline;

	boolean sacrificing;
	boolean demonAnimationGoing;
	int selectedSac = 0;
	float sacTime = 0;

	ArrayList<Animal> possibleSacrifices;
	Altar currentAltar;

	CameraTarget target;
	Sprite demonicText;

	boolean shouldFlash;

	Music music;

	boolean showingSign;
	String signText;

	boolean endgame;
	float endgameCycler;
	int endgameState;
	
	Array<String> killedDudes = new Array<>();
	String inMemoryOf;
	
	Sound endgameSmack;
	
	class BackgroundBlock {
		Sprite s;
		float x,y,w,h;
		
		public BackgroundBlock() {
			s = getSprite("doot_black");
			x = MathUtils.random(level.getWidth()) / 2;
			y = (level.groundHeightAt(x * 2)) * Level.BLOCK_WIDTH;
			w = MathUtils.random(30, 500);
			h = MathUtils.random(65, 300);
			s.setAlpha(MathUtils.random(0.1f));
			s.setPosition(x, y);
			s.setSize(w, h);
		}

		public void render(OrthographicCamera camera, SpriteBatch worldBatch) {
			float nx = x + camera.position.x / 2;
			s.setX(x + camera.position.x / 2);
			Vector3 screenCoords = camera.project(new Vector3(nx, y, 0));
			if(screenCoords.x > -w*2 && screenCoords.x < Gdx.graphics.getWidth()) {
				s.draw(worldBatch);
			}
		}
	}
	
	Array<BackgroundBlock> backgroundBlocks;

	LD43Game game;
	
	float musicVol = 0.8f;
	
	public PlayScreen(LD43Game game) {
		this.game = game;
		
		atlas = new TextureAtlas(Gdx.files.internal("m2pac-packed/pack.atlas"));

		player = new Player(this);

		level = new Level(this, player);
		worldBatch = new SpriteBatch();
		hudBatch = new SpriteBatch();
		demonBatch = new SpriteBatch();
		debugRenderer = new ShapeRenderer();
		camera = new OrthographicCamera(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);

		hud = new HUD(player, this);

		standardFont = new BitmapFont(Gdx.files.internal("pixeled.fnt"));
		cursedFont = new BitmapFont(Gdx.files.internal("homocide.fnt"));

		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(0x00000044);
		pixmap.drawPixel(0, 0);
		dimmingBlock = new Texture(pixmap);

		Pixmap pixmap2 = new Pixmap(1, 1, Format.RGBA8888);
		pixmap2.setColor(0xffffffff);
		pixmap2.drawPixel(0, 0);
		lightenBlock = new Texture(pixmap2);

		sacrificeBackground = new NinePatch(atlas.findRegion("sacrifice_dialog"));
		outline = new NinePatch(atlas.findRegion("outline_nine"), 1, 1, 1, 1);

		target = player;

		demonicText = getSprite("satanic_text");

		music = Gdx.audio.newMusic(Gdx.files.internal("song1.ogg"));
		music.setVolume(musicVol);
		music.setLooping(true);
		musicFadeIn();

		cursedGlyph = new GlyphLayout(cursedFont, "???");

		camera.position.set(level.getPlayerSpawnX(), level.getPlayerSpawnY(), 0);
		
		endgameSmack = Gdx.audio.newSound(Gdx.files.internal("endgame_smack.ogg"));
		
		//	Background?
		backgroundBlocks = new Array<>();
		for(int u = 0; u < 100; u++) {
			backgroundBlocks.add(new BackgroundBlock());
		}
	}

	public TextureRegion getTextureRegion(String name) {
		return atlas.findRegion(name);
	}

	public Sprite getSprite(String name) {
		return atlas.createSprite(name);
	}

	public String fuckText(String text) {
		String result = "";
		for (char c : text.toCharArray()) {
			if (c != ' ' && MathUtils.randomBoolean(0.05f))
				c += MathUtils.random(-5, 5);
			result += c;
		}
		return result;
	}
	
	long smackID;
	float timeInEndgame;

	@Override
	public void render(float dt) {
		if (endgame) {
			LD43Game.a = MathUtils.random(0.05f);
			timeInEndgame += dt;
			if(timeInEndgame > 2) {
				endgameCycler += dt;
				
				if(endgameCycler > 0.8f) {
					endgameCycler = 0;
					endgameState++;
					endgameState %= 5;
					
					if(endgameState == 2) {
						inMemoryOf = killedDudes.get(MathUtils.random(killedDudes.size - 1));
					}
				}
				
				hudBatch.begin();
				if(MathUtils.randomBoolean(0.02f)) hudBatch.draw(lightenBlock, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				
				int j = (int) MathUtils.randomTriangular(-2, 2);
				
				if(endgameState == 0) {
					cursedGlyph.setText(cursedFont, "Oblation");
					cursedFont.draw(hudBatch, "Oblation", 
							Gdx.graphics.getWidth() / 4 + j, 
							Gdx.graphics.getHeight() / 2, 
							Gdx.graphics.getWidth() / 2, Align.center, true);
				} else if(endgameState == 1) {
					cursedFont.draw(hudBatch, "a game by Sam Hindson", 
							Gdx.graphics.getWidth() / 4 + j, 
							Gdx.graphics.getHeight() / 2, 
							Gdx.graphics.getWidth() / 2, Align.center, true);
				} else if(endgameState == 2) {
					cursedFont.draw(hudBatch, "In loving memory of\n" + inMemoryOf, 
							Gdx.graphics.getWidth() / 4 + j, 
							Gdx.graphics.getHeight() / 2 + 10, 
							Gdx.graphics.getWidth() / 2, Align.center, true);
				} else if(endgameState == 3) {
					cursedFont.draw(hudBatch, "Press J", 
							Gdx.graphics.getWidth() / 4 + j, 
							Gdx.graphics.getHeight() / 2 + 10, 
							Gdx.graphics.getWidth() / 2, Align.center, true);
				} else if(endgameState == 4) {
					cursedFont.draw(hudBatch, fuckText("Oblation"), 
							Gdx.graphics.getWidth() / 4 + j, 
							Gdx.graphics.getHeight() / 2, 
							Gdx.graphics.getWidth() / 2, Align.center, true);
				}
				
				hudBatch.end();
				
				if(Gdx.input.isKeyJustPressed(Keys.J)) {
					game.openMenuScreen();
				}
				
			} else {
				hudBatch.begin();
				hudBatch.draw(lightenBlock, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				hudBatch.end();
			}
			
		} else {
			// Musak
			if (musicFadingIn) {
				fadeProgress += dt;
				music.setVolume(musicVol * (fadeProgress / 2f));

				if (fadeProgress > 2) {
					fadeProgress = 0;
					music.setVolume(musicVol);
					musicFadingIn = false;
				}
			} else if (musicFadingOut) {
				fadeProgress += dt;
				music.setVolume(musicVol * ((0.4f - fadeProgress) / 0.4f));

				if (fadeProgress > 0.4f) {
					fadeProgress = 0;
					music.setVolume(0);
					music.pause();
					musicFadingOut = false;
				}
			}

			if (demonAnimationGoing) {
				camera.zoom *= 0.995f;

				if (!shouldFlash)
					shouldFlash = MathUtils.randomBoolean(0.09f);
			} else {
				camera.zoom += (1.5f - camera.zoom) / 10;
			}

			// Game FACTS and LOGIC
			if (!sacrificing) {
				player.update(dt, level);
			}

			level.update(dt, camera);
			
			if(endgame) {
				smackID = endgameSmack.play(0.2f);
				//endgameMusic.play();
				//endgameMusic.setLooping(true);
				return;	
			}

			float cdx = target.getCenterX() - camera.position.x;
			float cdy = target.getCenterY() - camera.position.y;
			camera.translate(cdx / 20, cdy / 20);

			if (demonAnimationGoing) {
				camera.translate(MathUtils.random(-1, 1), MathUtils.random(-1, 1));
			}

			if (notificationTime > 0) {
				notificationTime -= dt;
			} else {
				notificationTime = 0;
			}

			// Debug cam stuff
			/*
			 * if(Gdx.input.isKeyPressed(Keys.LEFT)) camera.translate(-dt * 500,
			 * 0); if(Gdx.input.isKeyPressed(Keys.RIGHT)) camera.translate(-dt *
			 * -500, 0); if(Gdx.input.isKeyPressed(Keys.UP)) camera.translate(0,
			 * dt * 500); if(Gdx.input.isKeyPressed(Keys.DOWN))
			 * camera.translate(0, dt * -500);
			 * if(Gdx.input.isKeyPressed(Keys.PAGE_UP)) camera.zoom += dt * 10;
			 * if(Gdx.input.isKeyPressed(Keys.PAGE_DOWN)) camera.zoom -= dt *
			 * 10;
			 */

			camera.update();
			demonBatch.setProjectionMatrix(camera.combined);
			worldBatch.setProjectionMatrix(camera.combined);
			debugRenderer.setProjectionMatrix(camera.combined);

			worldBatch.begin();
			
			for(BackgroundBlock block : backgroundBlocks) {
				block.render(camera, worldBatch);
			}
			
			level.render(worldBatch, camera);
			player.render(worldBatch);
			if (demonAnimationGoing) {
				worldBatch.draw(dimmingBlock, camera.position.x - 800, camera.position.y - 800, 1600, 1600);
			}
			worldBatch.end();

			/*
			debugRenderer.begin(ShapeType.Line);
			level.debugRender(debugRenderer);
			player.debugRender(debugRenderer); debugRenderer.end();
			*/
			

			// HUD
			hudBatch.begin();

			// Sacrificing dialog
			if (sacrificing) {
				sacTime += dt;
				hudBatch.draw(dimmingBlock, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

				int dh = Gdx.graphics.getHeight() / 2;

				sacrificeBackground.draw(hudBatch, Gdx.graphics.getWidth() / 2 - dh / 2,
						Gdx.graphics.getHeight() / 2 - dh / 2, dh, dh);

				if (Gdx.input.isKeyJustPressed(Keys.W) && selectedSac != 0) {
					selectedSac--;
				} else if (Gdx.input.isKeyJustPressed(Keys.S) && selectedSac != possibleSacrifices.size() - 1) {
					selectedSac++;
				}

				int q = 0;
				for (Animal animal : possibleSacrifices) {
					int bx = (int) (Gdx.graphics.getWidth() / 2 - dh / 2 + dh * 0.1f);
					int by = (int) (Gdx.graphics.getHeight() / 2 + dh / 2 - dh * 0.1f - (q + 1) * 40);

					if (q == selectedSac)
						outline.draw(hudBatch, bx, by, dh * 0.8f, 40);

					q++;

					hudBatch.draw(atlas.findRegion("icon_" + animal.getName()), bx + 4, by + 4, 32, 32);

					standardFont.draw(hudBatch, animal.getPetName(), bx + 50, by + 40 - 5);
					standardFont.draw(hudBatch, animal.getBiography(), bx + 50,
							by + standardFont.getLineHeight() * 0.7f);
				}
				
				cursedFont.draw(hudBatch, "[J] Offer\n[K] Cancel", Gdx.graphics.getWidth() / 3 - 20, 120, dh, Align.center, false);

				if (Gdx.input.isKeyJustPressed(Keys.J) && sacTime > 0.1f) {
					musicFadeOut();
					target = possibleSacrifices.get(selectedSac);
					possibleSacrifices.get(selectedSac).sacrifice(currentAltar);
					sacrificing = false;
				}

				if (Gdx.input.isKeyJustPressed(Keys.K))
					sacrificing = false;
			} else if (!demonAnimationGoing) {
				hud.render(hudBatch);

				// Notifications
				if (notificationTime > 0) {
					standardFont.draw(hudBatch, notificationText, 10, ICONSIZE + 20 + standardFont.getLineHeight());
				}
			}

			// Demon madness
			if (demonAnimationGoing) {
				demonicText.setAlpha(MathUtils.random(0.1f, 0.3f));
				demonicText.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth() / 0.75f);
				demonicText.draw(hudBatch);
			}

			// Batch batch, CharSequence str, float x, float y, float
			// targetWidth, int halign, boolean wrap

			if (shouldFlash) {
				shouldFlash = false;
				hudBatch.draw(lightenBlock, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			}

			// Show signs
			if (showingSign) {

				float bh = 85;

				float bx = Gdx.graphics.getWidth() / 2 - 200;
				float by = Gdx.graphics.getHeight() - 50 - bh;
				
				hudBatch.draw(dimmingBlock, bx, by, 400, bh);

				cursedFont.draw(hudBatch, signText, bx + 10, by + bh - 10, 360, Align.center, true);
			}

			hudBatch.end();

			if (demonAnimationGoing) {
				demonBatch.begin();
				currentAltar.drawDemon(demonBatch);
				demonBatch.end();
			}
		}
	}

	@Override
	public void show() {

	}

	public void dispose() {
		// ?
		music.dispose();
		standardFont.dispose();
		cursedFont.dispose();
		endgameSmack.dispose();
		lightenBlock.dispose();
		dimmingBlock.dispose();
		atlas.dispose();
	}

	public void makeNotification(String text) {
		notificationText = text;
		notificationTime = 5;
	}

	public void beginSacrifice(Altar altar, ArrayList<Animal> potentials) {
		sacTime = 0;
		selectedSac = 0;
		possibleSacrifices = potentials;
		sacrificing = true;
		currentAltar = altar;
	}

	public void finishSacrifice() {
		target = currentAltar;
		currentAltar.demonActivate();
	}

	public void refocusPlayer() {
		musicFadeIn();
		shouldFlash = true;
		if (demonAnimationGoing) {
			target = player;
			demonAnimationGoing = false;
		}
	}

	boolean musicFadingIn = true;
	boolean musicFadingOut = false;
	float fadeProgress = 0;

	private void musicFadeIn() {
		if (music.isPlaying())
			return;
		musicFadingIn = true;
		fadeProgress = 0;
		music.play();
	}

	public void musicFadeOut() {
		musicFadingOut = true;
		fadeProgress = 0;
	}

	public void demonAnimationBegin() {
		demonAnimationGoing = true;
		shouldFlash = true;
	}

	public TextureAtlas getAtlas() {
		return atlas;
	}

	public void showSign(String text) {
		signText = text;
		showingSign = true;
	}

	public void hideSign() {
		showingSign = false;
	}

	public void endgame() {
		music.stop();
		//level.destroyEntities();
		endgame = true;
	}

	public void addInMemoryOf(String string) {
		killedDudes.add(string);
	}

	public boolean isSacrificing() {
		return sacrificing;
	}
}
