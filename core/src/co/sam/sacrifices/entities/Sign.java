package co.sam.sacrifices.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public class Sign extends Entity {
	
	//	Bad things
	//	every 
	//	body
	//	does
	//	
	//	bad 
	// things
	static final String[] signTexts = {
		"Fear not the unknown - press ever onward.",
		"Hast though an offering? Duck, perhaps?",
		"Beckon these flightless birds follow you. [J]",
		"Do not be afraid to look back.",
		"Dead weight is no good - move on.",
		"Do not be afraid to make new friends...",
		"...but equally, remember to take things one step at a time.",
		"It is important to remember your peers' limitations.",
		"Be wary of picky eaters in this realm.", 
		"To self-sabotage is to be human.",
		"Here you lie:\nMagnus Rylock.",
		"Born 5565 AD,\nDied 12 ED",
		"You are damned to roam the land of fire beyond this.",
		"Perform the final sacrifice."
	};

	int id;
	
	@Override
	public void reset() {
		
	}
	
	public Sign(Level level, PlayScreen host, float x2, float y2, int id) {
		super(level, host, x2, y2);
		w = h = 24;
		name = "sign";
		sprite = host.getSprite("sign");
		sprite.setPosition(x2, y2);
		this.id = id;
	}

	@Override
	public boolean isActivatable() {
		return false;
	}

	@Override
	public void update(float dt, Level level) {
		
	}

	@Override
	public void draw(SpriteBatch batch) {
		sprite.draw(batch);
	}

	@Override
	public void activate(Player player) {
		
	}
	
	@Override
	public void hover(Level level, PlayScreen host) {
		host.showSign(signTexts[id]);
	}
	
	@Override
	public void unhover(Level level, PlayScreen host) {
		host.hideSign();
	}
	
	@Override
	public void destroy() {
		
	}
}
