package co.sam.sacrifices.entities;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public class Altar extends Entity {

	public static final String[] NAMES = { "Domeron", "Akanon", "Rhlyagon", "Dorgon", "Laeron", "Kerogash", "L'engdor",
			"Mammon", "Kassian", "Vulag", "Mr'ai", "Sugondese", "Gomron", "Ket", "Karmon", "Kesla'thor", "Deg",
			"Baphomet" };

	public static final String[] FOOD = { "Roast Duck", "Tender Mutton", "Seared Beef" };

	static final int ANY = 6;

	protected String demonName;

	int id;
	int needs;

	static final float ANIM_DURATION = 2f;

	boolean playingDemonAnimation;
	float animtaionProgress;

	boolean spent = false;

	Sprite demonSprite;
	Demonic demonicEntity;

	ParticleEffect roastAnimalEffect;

	static Sound sacrificeEffect1, sacrificeEffect2, sacrificeEffect3;

	public Altar(Level level, PlayScreen host, float x, float y, int id, int needs) {
		super(level, host, x, y);
		name = "altar";
		w = 64;
		h = 20;
		sprite = host.getSprite("altar");
		sprite.setSize(w, h);
		sprite.setPosition(x, y);

		this.id = id;
		this.needs = needs;

		demonName = NAMES[id];

		demonSprite = host.getSprite("demon" + (id % 3 + 1));
		demonSprite.setOriginCenter();
		demonSprite.setX(getCenterX() - demonSprite.getWidth() / 2);
		demonSprite.setY(getCenterY());
		demonSprite.setScale(2);

		roastAnimalEffect = new ParticleEffect();
		roastAnimalEffect.load(Gdx.files.internal("roast_animal.p"), host.getAtlas());

		if(sacrificeEffect1 == null) {
			sacrificeEffect1 = Gdx.audio.newSound(Gdx.files.internal("sacrifice1.ogg"));
			sacrificeEffect2 = Gdx.audio.newSound(Gdx.files.internal("sacrifice2.ogg"));
			sacrificeEffect3 = Gdx.audio.newSound(Gdx.files.internal("sacrifice3.ogg"));
		}
	}

	@Override
	public void reset() {
		spent = false;
		playingDemonAnimation = false;
		animtaionProgress = 0;
		demonicEntity.reset();
	}

	public void setLinkedEntity(Demonic d) {
		demonicEntity = d;
	}

	@Override
	public void update(float dt, Level level) {
		if (playingDemonAnimation) {
			animtaionProgress += dt;

			if (animtaionProgress <= 0.3f) {
				demonSprite.setAlpha(animtaionProgress / 0.3f);
			} else if (animtaionProgress > 0.3f && animtaionProgress < ANIM_DURATION - 0.3f) {
				demonSprite.setAlpha(1);
			} else {
				demonSprite.setAlpha(Math.max((ANIM_DURATION - animtaionProgress) / 0.3f, 0));
			}

			demonSprite.setOriginCenter();
			demonSprite.setX(getCenterX() - demonSprite.getWidth() / 2);
			demonSprite.setY(getCenterY());

			if (animtaionProgress > ANIM_DURATION) {
				host.refocusPlayer();
				playingDemonAnimation = false;
			}
		}
	}

	@Override
	public float getCenterY() {
		return getY() + getH() / 2 + getH() * 3 * (animtaionProgress / ANIM_DURATION);
	}

	@Override
	public void draw(SpriteBatch batch) {
		sprite.draw(batch);

		roastAnimalEffect.draw(batch, Gdx.graphics.getDeltaTime());
	}

	public void drawDemon(SpriteBatch demonBatch) {
		if (playingDemonAnimation) {
			int o = (int) MathUtils.randomTriangular(-1, 1);
			demonSprite.setX(demonSprite.getX() + o);
			demonSprite.draw(demonBatch);
			demonSprite.setX(demonSprite.getX() - o);
		}
	}

	@Override
	public void activate(Player player) {
		if (spent) {
			host.makeNotification(demonName + "is satisfied with what you have provided... For now.");
			return;
		}
		if (player.getFollowers().isEmpty()) {
			host.makeNotification("Do not waste " + demonName + "'s time and patience.");
		} else {
			ArrayList<Animal> potentials;
			if (needs == ANY) {
				potentials = player.getFollowers();
				host.beginSacrifice(this, potentials);
			} else {
				potentials = new ArrayList<>();
				for (Animal animal : player.getFollowers()) {
					if (animal.getKind() >= needs) {
						potentials.add(animal);
					}
				}
				if (potentials.isEmpty()) {
					String minimum = FOOD[needs - 7];
					host.makeNotification(demonName + " will settle for nothing less than " + minimum + ".");
				} else {
					host.beginSacrifice(this, potentials);
				}
			}
		}
	}

	public int getID() {
		return id;
	}

	@Override
	public boolean isActivatable() {
		return true;
	}

	public void demonActivate() {
		if (!spent) {
			spent = true;
			playingDemonAnimation = true;
			host.demonAnimationBegin();
			demonicEntity.demonActivate(level);
			animtaionProgress = 0;
			roastAnimalEffect.setPosition(getCenterX() - 16, y + h - 3);
			roastAnimalEffect.start();
			if (needs != Animal.HUMAN) {
				int r = id % 3 + 1;
				if(r==1) sacrificeEffect1.play(0.5f);
				if(r==2) sacrificeEffect2.play(0.5f);
				if(r==3) sacrificeEffect3.play(0.5f);
			}
		}
	}

	@Override
	public void destroy() {
		sacrificeEffect1.stop();
		sacrificeEffect1.dispose();
		sacrificeEffect2.stop();
		sacrificeEffect2.dispose();
		sacrificeEffect3.stop();
		sacrificeEffect3.dispose();
		roastAnimalEffect.dispose();
	}

}
