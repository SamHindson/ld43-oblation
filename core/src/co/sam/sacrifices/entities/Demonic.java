package co.sam.sacrifices.entities;

import co.sam.sacrifices.level.Level;

public interface Demonic {
	public void demonActivate(Level level);
	public int getID();
	public void reset();
}
