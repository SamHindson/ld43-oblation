package co.sam.sacrifices.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.screens.PlayScreen;

public class Duck extends Animal {
	
	static Sound quack1;
	static Sound quack2;
	static Sound sacrifice;
	
	float pitch;
	
	public Duck(Level level, PlayScreen host, float x, float y) {
		super(level, host, x ,y);
		w = h = 16;
		name = "duck";
		speed = MathUtils.random(150, 180);
		followDistance = MathUtils.random(w, w * 1.5f);
		sprite = host.getSprite("duck");
		sprite.setSize(w, h);
		
		if(quack1 == null) {
			quack1 = Gdx.audio.newSound(Gdx.files.internal("duck1.ogg"));
			quack2 = Gdx.audio.newSound(Gdx.files.internal("duck2.ogg"));
			sacrifice = Gdx.audio.newSound(Gdx.files.internal("duckS.ogg"));
		}

		walkAnimation = new Animation<TextureRegion>(0.05f, host.getAtlas().findRegions("duck_w"), PlayMode.LOOP_PINGPONG);
		idleAnimation = new Animation<TextureRegion>(0.5f, host.getAtlas().findRegions("duck_idle"), PlayMode.LOOP_PINGPONG);
		
		pitch = MathUtils.random(1.3f, 2f);
	}
	
	long quack;

	@Override
	public void update(float dt, Level level) {
		super.update(dt, level);
		
		if(following && active) {
			if(MathUtils.randomBoolean(0.01f)) {
				if(MathUtils.randomBoolean()) {
					quack = quack1.play();
					quack1.setVolume(quack, 0.2f);
					quack1.setPitch(quack, pitch);
				}
				else {
					quack = quack2.play();
					quack2.setVolume(quack, 0.2f);
					quack2.setPitch(quack, pitch);
				}
			}
		}
	}
	
	@Override
	protected void squeal() {
		long s = sacrifice.play();
		sacrifice.setVolume(s, 0.4f);
	}
	
	@Override
	protected boolean shouldWander() {
		return true;
	}
	
	@Override
	public int getKind() {
		return Animal.DUCK;
	}
	
	@Override
	protected void jump() {
		return;
	}
	
	@Override
	protected float getFallThreshold() {
		return -900;
	}
}
