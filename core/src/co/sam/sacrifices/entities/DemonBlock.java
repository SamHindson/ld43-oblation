package co.sam.sacrifices.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public class DemonBlock extends Entity implements Demonic {

	int id;
	boolean spent = false;
	Sprite sprite;

	public DemonBlock(Level level, PlayScreen host, float x2, float y2, int id) {
		super(level, host, x2, y2);
		name = "demonblock";
		w = h = Level.BLOCK_WIDTH;
		this.id = id;
		sprite = host.getSprite("demonblock");
		sprite.setPosition(x2, y2);
		sprite.setOriginCenter();
	}
	
	@Override
	public void reset() {
		spent = false;
	}

	@Override
	public void demonActivate(Level level) {
		level.addFloorBlock(x, y);
		level.makeCreationCloud(x + w / 2, y + h / 2);
	}

	@Override
	public boolean isActivatable() {
		return false;
	}

	@Override
	public void update(float dt, Level level) {
		if (!spent) {
			sprite.setAlpha(MathUtils.random(0.1f, 0.5f));
			sprite.setScale(MathUtils.random(0.7f, 0.9f));
		}
	}

	@Override
	public void draw(SpriteBatch batch) {
		if(!spent)
			sprite.draw(batch);
	}

	@Override
	public void activate(Player player) {

	}

	@Override
	public int getID() {
		return id;
	}
	
	@Override
	public void destroy() {
		
	}

}
