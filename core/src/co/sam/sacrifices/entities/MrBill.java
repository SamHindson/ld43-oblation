package co.sam.sacrifices.entities;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.screens.PlayScreen;

//	No offense to mr bill
//	i just thought the sprite looked the same as his logo

public class MrBill extends Animal {

	public MrBill(Level level, PlayScreen host, float x, float y) {
		super(level, host, x, y);
		name = "mrbill";
		w = h = Level.BLOCK_WIDTH;
		speed = 100;
		followDistance = MathUtils.random(w, w * 2f);
		sprite = host.getSprite("broski");
		sprite.setSize(w, h);

		walkAnimation = new Animation<TextureRegion>(0.05f, host.getAtlas().findRegions("broski2_w"), PlayMode.LOOP_PINGPONG);
		idleAnimation = new Animation<TextureRegion>(0.1f, host.getAtlas().findRegions("broski2_idle"), PlayMode.LOOP);
	}

	@Override
	protected float getFallThreshold() {
		return Integer.MIN_VALUE;
	}
	
	@Override
	public void update(float dt, Level level) {
		super.update(dt, level);
	}
	
	@Override
	protected boolean shouldWander() {
		return false;
	}

	@Override
	protected void jump() {
		return;
	}
	
	@Override
	public String getPetName() {
		return "You";
	}

	@Override
	public String getBiography() {
		return "You killed her";
	}
	
	@Override
	public int getKind() {
		return HUMAN;
	}
	
	@Override
	protected void squeal() {
		// TODO Auto-generated method stub
		
	}

}
