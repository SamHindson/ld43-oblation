package co.sam.sacrifices.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public class RockPile extends Entity implements Demonic {

	class Rock {
		Sprite sprite;
		boolean floating;
		float w, h;
		float omega, dw;
		float x, y, dx, dy;
		float fx, fy;
		float ox, oy;

		void beginFloat() {
			floating = true;
			fx = x + MathUtils.random(-5f, 5f);
			fy = MathUtils.random(3f, 5f) * Level.BLOCK_WIDTH + y;
			dw = MathUtils.random(-0.5f, 0.5f);
		}

		void update(float dt) {
			if (floating) {
				dx *= 0.9f;
				dy *= 0.9f;

				dx -= (x - fx) * dt;
				dy -= (y - fy) * dt;

				x += dx * dt;
				y += dy * dt;
				omega += dw;

				sprite.setPosition(x, y);
				sprite.setRotation(omega);
			}
		}
	}

	Rock[] rocks;
	int rockCount = 25;
	int id;
	boolean lifted = false;

	float apparitionTime = 0;
	boolean apparitioning = false;
	
	@Override
	public void reset() {
		lifted = false;
		for(Rock rock : rocks) {
			rock.floating = false;
			rock.x = rock.ox;
			rock.y = rock.oy;
			rock.dx = rock.dy = rock.dw = 0;
			rock.sprite.setPosition(rock.x, rock.y);
		}
	}

	public boolean isLifted() {
		return lifted;
	}

	public RockPile(Level level, PlayScreen host, float x2, float y2, int id) {
		super(level, host, x2, y2);

		this.id = id;

		name = "rockpile";
		w = 32 * 2;
		h = 32 * 2;

		rocks = new Rock[rockCount];
		for (int i = 0; i < rockCount; i++) {
			//float oy = MathUtils.randomTriangular(h/2) + h/2;
			float oy = ((i*1.0f)/rockCount * 1.0f) * h - Level.BLOCK_WIDTH / 2;
			float x = x2+w/2 + MathUtils.randomTriangular() * (h-oy) * 0.9f;
			Rock rock = new Rock();
			rock.ox = x;
			rock.oy = y2 + oy;
			rock.x = rock.ox;
			rock.y = rock.oy;
			rock.w = MathUtils.random(16, 48);
			rock.h = rock.w;
			int var = MathUtils.random(1, 3);
			rock.sprite = host.getSprite("rock" + var);
			//rock.sprite.setRotation(MathUtils.random(360));
			rock.sprite.setOriginCenter();
			rock.sprite.setPosition(rock.x, rock.y);
			rock.sprite.setSize(rock.w, rock.h);
			rocks[i] = rock;
		}
	}
	
	@Override
	public boolean isActivatable() {
		return !lifted;
	}
	
	@Override
	public void hover(Level level, PlayScreen host) {
		
	}

	@Override
	public void demonActivate(Level level) {
		for (Rock r : rocks) {
			r.beginFloat();
		}
		lifted = true;
	}

	@Override
	public void update(float dt, Level level) {
		if (apparitioning) {
			apparitioning = (apparitionTime += dt) < 1.3f;
		}

		for (Rock r : rocks) {
			r.update(dt);
		}
	}

	@Override
	public void draw(SpriteBatch batch) {
		for (Rock r : rocks) {
			r.sprite.draw(batch);
		}
	}

	@Override
	public void activate(Player player) {
		apparitionTime = 0;
		apparitioning = true;
	}

	public int getID() {
		return id;
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
