package co.sam.sacrifices.entities;

public interface CameraTarget {
	public float getCenterX();
	public float getCenterY();
}
