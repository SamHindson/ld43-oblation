package co.sam.sacrifices.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.screens.PlayScreen;

public class Goat extends Animal {

	static Sound bah1;
	static Sound bah2;
	static Sound sacrifice;
	
	float pitch = MathUtils.random(1f, 2.3f);

	public Goat(Level level, PlayScreen host, float x, float y) {
		super(level, host, x, y);
		w = h = 32;
		name = "goat";
		speed = MathUtils.random(150, 180);
		followDistance = MathUtils.random(w, w * 1.5f);
		sprite = host.getSprite("goat");
		sprite.setSize(w, h);

		if (bah1 == null) {
			bah1 = Gdx.audio.newSound(Gdx.files.internal("goat1.ogg"));
			bah2 = Gdx.audio.newSound(Gdx.files.internal("goat2.ogg"));
			sacrifice = Gdx.audio.newSound(Gdx.files.internal("goatS.ogg"));
		}
		
		walkAnimation = new Animation<TextureRegion>(0.05f, host.getAtlas().findRegions("goat_w"), PlayMode.LOOP_PINGPONG);
		idleAnimation = new Animation<TextureRegion>(0.2f, host.getAtlas().findRegions("goat_idle"), PlayMode.LOOP_PINGPONG);
	}
	
	long bah;
	
	@Override
	public void update(float dt, Level level) {
		// TODO Auto-generated method stub
		super.update(dt, level);
		if(following && active) {
			if(MathUtils.randomBoolean(0.0025f)) {
				if(MathUtils.randomBoolean()) {
					bah = bah1.play();
					bah1.setVolume(bah, 0.2f);
					bah1.setPitch(bah, pitch);
				}
				else {
					bah = bah2.play();
					bah2.setVolume(bah, 0.2f);
					bah2.setPitch(bah, pitch);
				}
			}
		}
	}

	@Override
	protected boolean shouldWander() {
		return true;
	}

	@Override
	public int getKind() {
		return Animal.GOAT;
	}

	@Override
	protected void jump() {
		dy = 150;
	}

	@Override
	protected float getFallThreshold() {
		return -320;
	}

	@Override
	protected void squeal() {
		long s = sacrifice.play();
		sacrifice.setVolume(s, 04f);
	}
}
