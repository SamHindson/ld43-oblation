package co.sam.sacrifices.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public abstract class Animal extends Entity {

	public static final String[] NAMES = { "Steven", "Michael", "Marcus", "Clayton", "Max", "Gregory", "Charlie",
			"Alexander", "Oliver", "Liam", "Jonathan", "Sebastian", "Rover", "Butch", "Fluffy", "Lawrence", "Adam",
			"Bruce", "Joel", "Sean", "James", "John", "Daniel", "Matt", "Jacob", "Bones",

			"Sandy", "Caitlin", "Georgi", "Leanne", "Megan", "Katie", "Maxene", "Emmy", "Alexandra", "Cleopatra",
			"Lauren", "Elyse", "Alanah",

			"Mr Cuddles" };

	public static final String[] BIOS = { 
			"Single parent", 
			"Parent of 2", 
			"Single parent of 3", 
			"Parent of 4",
			"Parent of 5", 
			"Parent of 8", 
			"Philanthropist", 
			"Recently married", 
			"Recently divorced", 
			"Inventor",
			"Loving Life",
			"Recent Graduate",
			"Physicist",
			"Plays videogames",
			"LOTR Enthusiast",
			"/r/saltierthancrait mod",
			"/r/dankmemes mod",
			"Cook",
			"Family Man",
			"Paramedic",
			"Social Worker",
			"Ex-president",
			"Volunteer Firefighter" };

	protected String petName;
	protected String biography;

	protected boolean following;

	protected boolean falling;
	protected float speed;
	protected float followDistance;
	protected int travelDir = -1;

	protected boolean wandering = true;
	protected float actionTime, currentActionTime;
	protected boolean walking = false;

	protected boolean beingSacrificed;

	static final int DUCK = 7;
	static final int GOAT = 8;
	static final int HUMAN = 9;

	protected float omega = 0;
	protected float dw;

	protected Altar altar;
	protected Player player;

	protected static ParticleEffect oofCloud;

	protected static Sound oof;
	
	protected Animation<TextureRegion> walkAnimation;
	protected Animation<TextureRegion> idleAnimation;
	
	float animDuration;

	@Override
	public void reset() {
		falling = true;
		active = true;
		beingSacrificed = false;
		actionTime = 0;
		currentActionTime = 0;
		sprite.setRotation(0);
		following = false;
		player = null;
		x = ox;
		y = oy;
		dx = dy = 0;
	}

	public Animal(Level level, PlayScreen host, float x, float y) {
		super(level, host, x, y);
		falling = true;
		petName = NAMES[MathUtils.random(NAMES.length - 1)];
		biography = BIOS[MathUtils.random(BIOS.length - 1)];

		if (oof == null) {
			oof = Gdx.audio.newSound(Gdx.files.internal("oof.ogg"));
		}
	}

	public String getBiography() {
		return biography;
	}

	protected abstract boolean shouldWander();

	@Override
	public void update(float dt, Level level) {
		animDuration += dt;
		
		if (!active)
			return;
		if (beingSacrificed) {
			dy -= 300 * dt;
			omega += dw * dt;
			sprite.setRotation(omega);
			float sepX = -((x + w / 2) - (altar.getX() + altar.getW() / 2));
			if (Math.abs(sepX) < 5) {
				host.addInMemoryOf(getPetName() + getTypeString());
				die(level);
				host.finishSacrifice();
			}
		} else {
			if (falling && !level.canFall(x, y, dx, dy, w, h)) {
				if (dy < getFallThreshold()) {
					long os = oof.play();
					oof.setVolume(os, 0.05f);
					die(level);
				}
			}

			falling = level.canFall(x, y, dx, dy, w, h);

			if (falling) {
				dy -= 300 * dt;
			} else {
				dy = 0;
			}

			if (following) {
				float sepX = Math.abs(x - player.getX());
				if (sepX > Level.BLOCK_WIDTH * 5) {
					player.removeFollower(this);
					following = false;
					player = null;
					dx = 0;
				}
			} else {
				if (shouldWander()) {
					currentActionTime += dt;
					if (currentActionTime > actionTime) {
						wandering = !wandering;
						currentActionTime = 0;
						actionTime = MathUtils.random(0.5f, 2.3f);

						if (wandering) {
							dx = MathUtils.random(-0.2f, 0.2f) * speed;
						} else {
							dx = 0;
						}
					}
				}
			}

			if (wandering) {
				if (!level.canTraverse(x, y, w)[1] || !level.canTraverse(x, y, w)[0]) {
					wandering = false;
					currentActionTime = 0;
					dx = 0;
				}
			}

			if (dx < 0)
				sprite.setFlip(true, false);
			if (dx > 0)
				sprite.setFlip(false, false);

			if (!(level.canTraverse(x, y, w)[0]) || !(level.canTraverse(x, y, w)[1]))
				jump();

			if (dx > 0)
				dx = level.canTraverse(x, y, w)[1] ? dx : 0;
			else if (dx < 0)
				dx = level.canTraverse(x, y, w)[0] ? dx : 0;
			else
				dx = 0;

			if (host.isSacrificing())
				dx = 0;
		}

		x += dx * dt;
		y += dy * dt;

		sprite.setPosition(x, y);
	}

	private void die(Level level) {
		active = false;
		level.killAnimal(this);
		if (player != null) {
			player.removeFollower(this);
		}
	}

	protected abstract void squeal();

	protected abstract float getFallThreshold();

	protected abstract void jump();

	public void moveTo(float x) {
		if (beingSacrificed)
			return;
		if (Math.abs(x - this.x) < followDistance)
			dx = 0;
		else {
			travelDir = (int) Math.signum(x - this.x);
			dx = speed * travelDir;
		}
	}

	@Override
	public void draw(SpriteBatch batch) {
		int animOr = travelDir;
		if(animOr == 0) animOr = 1;
		float xo = animOr == -1 ? w : 0;
		
		if (!active)
			return;
		if(dx == 0) {
			batch.draw(idleAnimation.getKeyFrame(animDuration, true), x + xo, y, w * animOr, h);
		} else {
			batch.draw(walkAnimation.getKeyFrame(animDuration, true), x + xo, y, w * animOr, h);
		}
	}

	@Override
	public void activate(Player player) {
		player.addFollower(this);
	}

	public String getPetName() {
		return petName;
	}

	public abstract int getKind();

	public void sacrifice(Altar altar) {
		sprite.setOriginCenter();
		beingSacrificed = true;
		dw = MathUtils.random(-360f, 360f);
		dy = MathUtils.random(150, 250);
		float timeToImpact = 2 * dy / 300;
		float sepX = -((x + w / 2) - (altar.getX() + altar.getW() / 2));
		dx = sepX / timeToImpact;
		this.altar = altar;
		squeal();
	}

	public void setPlayer(Player player) {
		this.player = player;
		following = true;
	}

	@Override
	public boolean isActivatable() {
		return true;
	}

	@Override
	public void destroy() {
		if (oofCloud != null)
			oofCloud.dispose();
	}

	public String getTypeString() {
		if (getKind() == DUCK)
			return " the Duck";
		if (getKind() == GOAT)
			return " the Goat";
		return "";
	}
}
