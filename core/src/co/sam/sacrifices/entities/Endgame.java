package co.sam.sacrifices.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public class Endgame extends Entity implements Demonic {
	
	int id;

	public Endgame(Level level, PlayScreen host, float x2, float y2, int id) {
		super(level, host, x2, y2);
		name = "endgame";
		w = h = Level.BLOCK_WIDTH;
		this.id = id;
	}

	@Override
	public void demonActivate(Level level) {
		host.endgame();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public boolean isActivatable() {
		return false;
	}

	@Override
	public void update(float dt, Level level) {

	}

	@Override
	public void draw(SpriteBatch batch) {

	}

	@Override
	public void activate(Player player) {

	}
	
	@Override
	public void destroy() {
		// ;)
	}
	
	@Override
	public void reset() {
		// lol
	}

}
