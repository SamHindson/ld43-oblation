package co.sam.sacrifices.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.player.Player;
import co.sam.sacrifices.screens.PlayScreen;

public abstract class Entity implements CameraTarget {
	protected float x, y, dx, dy;
	protected float ox, oy;
	protected float w, h;
	protected Sprite sprite;

	protected PlayScreen host;
	
	protected Level level;
	
	protected boolean active = true;
	
	public Entity(Level level, PlayScreen host, float x2, float y2) {
		x = x2;
		y = y2;
		ox = x2;
		oy = y2;
		this.host = host;
		this.level = level;
	}
	
	public boolean isActive() {
		return active;
	}

	@Override
	public float getCenterX() {
		return getX() + getW() / 2;
	}
	
	@Override
	public float getCenterY() {
		return getY() + getH() / 2;
	}
	
	public abstract boolean isActivatable();
	
	public void hover(Level level) {}
	
	public abstract void update(float dt, Level level);
	public abstract void draw(SpriteBatch batch);
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getDx() {
		return dx;
	}
	public void setDx(float dx) {
		this.dx = dx;
	}
	public float getDy() {
		return dy;
	}
	public void setDy(float dy) {
		this.dy = dy;
	}
	public float getW() {
		return w;
	}
	public void setW(float w) {
		this.w = w;
	}
	public float getH() {
		return h;
	}
	public void setH(float h) {
		this.h = h;
	}
	
	public abstract void reset();
		
	protected String name;
	
	public String getName() {
		return name;
	}

	public void debugRender(ShapeRenderer debugRenderer) {
		debugRenderer.rect(x,y,w,h);
	}
	
	public abstract void activate(Player player);

	public void hover(Level level, PlayScreen host) {
		
	}
	
	public void unhover(Level level, PlayScreen host) {
		
	}

	public abstract void destroy();
	
}
