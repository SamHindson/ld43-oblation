package co.sam.sacrifices;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import co.sam.sacrifices.screens.MenuScreen;
import co.sam.sacrifices.screens.PlayScreen;

public class LD43Game extends ApplicationAdapter {

	private Screen currentScreen;

	public static float a = 0.7f;

	@Override
	public void create() {
		currentScreen = new MenuScreen(this);
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(a, a, a, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		currentScreen.render(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void dispose() {
		currentScreen.dispose();
	}

	public void openPlayScreen() {
		currentScreen.dispose();
		currentScreen = new PlayScreen(this);
	}
	
	public void openMenuScreen() {
		currentScreen.dispose();
		currentScreen = new MenuScreen(this);
	}
}
