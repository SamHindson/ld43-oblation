package co.sam.sacrifices.player;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import co.sam.sacrifices.LD43Game;
import co.sam.sacrifices.entities.Animal;
import co.sam.sacrifices.entities.CameraTarget;
import co.sam.sacrifices.entities.Entity;
import co.sam.sacrifices.level.Level;
import co.sam.sacrifices.screens.PlayScreen;

public class Player implements CameraTarget {
	static final float WALKSPEED = 200;

	float x, y, dx, dy;
	float w, h;
	
	PlayScreen host;

	boolean falling = false;

	ArrayList<Animal> followers;
	Entity hoveredEntity;

	float resetTimer;
	boolean resetting;

	Sound newFriend;

	int d;

	int walkProgress;
	float animDuration;
	boolean walking;
	
	//	Animation things?
	Animation<TextureRegion> walkAnimation;
	Animation<TextureRegion> idleAnimation;
	Animation<TextureRegion> fallAnimation;

	public Player(PlayScreen host) {
		this.host = host;

		x = 0;
		y = Level.BLOCK_WIDTH * 1;

		w = h = Level.BLOCK_WIDTH;

		falling = true;

		followers = new ArrayList<>();

		newFriend = Gdx.audio.newSound(Gdx.files.internal("newfriend.ogg"));
		
		d = 1;

		walkAnimation = new Animation<TextureRegion>(0.05f, host.getAtlas().findRegions("broski2_w"), PlayMode.LOOP_PINGPONG);
		idleAnimation = new Animation<TextureRegion>(0.5f, host.getAtlas().findRegions("broski2_idle"), PlayMode.LOOP_PINGPONG);
		fallAnimation = new Animation<TextureRegion>(0.1f, host.getAtlas().findRegions("broski2_f"), PlayMode.LOOP_PINGPONG);
	}

	public void update(float dt, Level level) {
		animDuration += dt;

		// Update darkness of the skaai
		LD43Game.a = 0.7f - (x / level.getWidth()) * 0.45f;

		// Am i falling
		falling = level.canFall(x, y, dx, dy, w, h);

		if (falling) {
			dy -= Level.GRAVITY * dt;
		} else {
			dy = 0;
			// y = (1 + level.groundHeightAt(x+w/2)) * Level.BLOCK_WIDTH;
			if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
				y += 5;
				dy = 150;
				falling = true;
			}
		}

		if (Gdx.input.isKeyPressed(Keys.R)) {
			resetting = true;
			resetTimer += dt;
			host.showSign("Coninue holding R to reset...");
			if (resetTimer >= 2) {
				resetTimer = 0;
				level.resetToCheckpoint(this);
			}
			return;
		} else {
			if (resetting)
				host.hideSign();
			resetting = false;
			resetTimer = 0;
		}

		// Traveling, man
		if (Gdx.input.isKeyPressed(Keys.D) && level.canTraverse(x, y, w)[1]) {
			dx = WALKSPEED;
			d = 1;
			walking = true;
		} else if (Gdx.input.isKeyPressed(Keys.A) && level.canTraverse(x, y, w)[0]) {
			dx = -WALKSPEED;
			d = -1;
			walking = true;
		} else {
			dx = 0;
			walking = false;
		}

		x += dx * dt;
		y += dy * dt;

		// Am i nearby any bitches
		if (hoveredEntity != null) {
			if (Math.abs(x + w / 2 - hoveredEntity.getX() - hoveredEntity.getW() / 2) > hoveredEntity.getW()) {
				hoveredEntity.unhover(level, host);
				hoveredEntity = null;
			}
		}

		Entity closest = null;
		float closestSep = 0;

		for (Entity e : level.getEntities()) {
			float sepX = Math.abs(x + w / 2 - e.getX() - e.getW() / 2);
			if (sepX < e.getW() && !followers.contains(e) && e.isActive()) {
				if (closest == null) {
					closest = e;
					closestSep = sepX;
				} else if (sepX < closestSep) {
					closest = e;
					closestSep = sepX;
				}
			}
		}

		if (closest != null) {
			if (hoveredEntity != closest && hoveredEntity != null)
				hoveredEntity.unhover(level, host);
			hoveredEntity = closest;
			closest.hover(level, host);
		}

		// Make do follow?
		if (Gdx.input.isKeyJustPressed(Keys.J) && hoveredEntity != null && hoveredEntity.isActivatable()) {
			hoveredEntity.activate(this);

			if (hoveredEntity instanceof Animal) {
				newFriend.setVolume(newFriend.play(), 0.5f);
			}

			hoveredEntity = null;
		}

		// Make sure followers know whats up
		for (Animal follower : followers) {
			follower.moveTo(x);
		}

		// Is there a peekaboo triggered
		if (Math.abs(x - level.getPeekabooX()) < 5) {
			level.peek();
		}
	}

	public void render(SpriteBatch batch) {
		float xo = d == -1 ? w : 0;
		if(walking)
			batch.draw(walkAnimation.getKeyFrame(animDuration, true), x + xo, y, w * d, h);
		else if(falling) {
			batch.draw(fallAnimation.getKeyFrame(animDuration, true), x + xo, y, w * d, h);
		} else {
			batch.draw(idleAnimation.getKeyFrame(animDuration, true), x + xo, y, w * d, h);
		}
	}

	public float getX() {
		return x;
	}

	public ArrayList<Animal> getFollowers() {
		return followers;
	}

	public float getY() {
		return y;
	}

	public void debugRender(ShapeRenderer debugRenderer) {
		debugRenderer.ellipse(x, y, 5, 5);
	}

	public Entity getHoveredEntity() {
		return hoveredEntity;
	}

	public void addFollower(Animal animal) {
		followers.add(animal);
		animal.setPlayer(this);
	}

	public void removeFollower(Animal animal) {
		for (Animal animal2 : followers) {
			if (animal2.equals(animal)) {
				followers.remove(animal2);
				break;
			}
		}
	}

	@Override
	public float getCenterX() {
		return getX() + w / 2;
	}

	@Override
	public float getCenterY() {
		return getY() + h * 2;
	}

	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;

	}

	public void clearFollowers() {
		hoveredEntity = null;
		followers.clear();
		host.hideSign();
	}
}
