package co.sam.sacrifices.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import co.sam.sacrifices.entities.Animal;
import co.sam.sacrifices.screens.PlayScreen;

public class HUD {

	Player player;
	PlayScreen host;

	public HUD(Player player, PlayScreen host) {
		this.player = player;
		this.host = host;
	}

	public void update(float dt) {

	}

	public void render(SpriteBatch batch) {
		int iconSize = PlayScreen.ICONSIZE;

		for (int i = 0; i < player.getFollowers().size(); i++) {
			Animal a = player.getFollowers().get(i);
			batch.draw(host.getTextureRegion("icon_" + a.getName()), Gdx.graphics.getWidth() - (i + 1) * iconSize, 10,
					iconSize, iconSize);
		}

		if (player.getHoveredEntity() != null && player.getHoveredEntity().isActivatable()) {
				batch.draw(host.getTextureRegion("icon_" + player.getHoveredEntity().getName()), 10, 10, iconSize,
						iconSize);
		}
	}

}
