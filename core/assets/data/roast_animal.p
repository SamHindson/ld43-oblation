Untitled
- Delay -
active: false
- Duration - 
lowMin: 12000.0
lowMax: 12000.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 250.0
highMax: 250.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.9019608
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.29452056
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.3
timelineCount: 3
timeline0: 0.0
timeline1: 0.66
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: line
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 36.0
highMax: 36.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 4.0
highMax: 4.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.7647059
scaling2: 0.11764706
timelineCount: 3
timeline0: 0.0
timeline1: 0.4520548
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 8.0
highMax: 70.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.33333334
timelineCount: 2
timeline0: 0.0
timeline1: 0.5890411
- Angle - 
active: true
lowMin: 90.0
lowMax: 90.0
highMin: 0.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 0.40392157
colors1: 0.6627451
colors2: 1.0
colors3: 1.0
colors4: 0.18431373
colors5: 0.047058824
colors6: 1.0
colors7: 0.18431373
colors8: 0.047058824
timelineCount: 3
timeline0: 0.0
timeline1: 0.16867469
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9298246
scaling2: 0.12280702
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.33561644
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/D:/Documents/Downloads/2pac/doot.png


Untitled
- Delay -
active: false
- Duration - 
lowMin: 12000.0
lowMax: 12000.0
- Count - 
min: 0
max: 25
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.8627451
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.3561644
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: line
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 36.0
highMax: 36.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 8.0
highMax: 32.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.10958904
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 90.0
highMax: 90.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 35.0
highMax: 55.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.37254903
colors1: 0.37254903
colors2: 0.37254903
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.05263158
scaling1: 0.47368422
scaling2: 0.05263158
timelineCount: 3
timeline0: 0.0
timeline1: 0.3219178
timeline2: 0.9246575
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
2pac/doot.png

